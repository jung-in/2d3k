package kr.codewiz.dd3k;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

import kr.codewiz.dd3k.controller.GameController;
import kr.codewiz.dd3k.data.Boss;
import kr.codewiz.dd3k.data.Card;
import kr.codewiz.dd3k.screen.IntroScreen;

public class MyGame extends Game {

    public SpriteBatch batch;
    public BitmapFont titleFont;

    @Override
    public void create() {

        GameController.Debug("MyGame - create()");

        if (GameController.isDebug)
            Gdx.app.setLogLevel(Application.LOG_DEBUG);
        else
            Gdx.app.setLogLevel(Application.LOG_NONE);

//        Gdx.input.setCatchMenuKey(true);
//
        batch = new SpriteBatch();

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font/arial.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 100;
        parameter.color = Color.BLACK;
        titleFont = generator.generateFont(parameter);
        generator.dispose();

        this.setScreen(new IntroScreen(this));
        GameController.getInstance().setBoss(new Boss());
        GameController.getInstance().getBoss().battlePoint = 200;

        getMyCard();

    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void resize(int width, int height) {
        GameController.Debug("MyGame - resize(width / height) : " + width + " / " + height);

        GameController.getInstance().scaleX = width / 1920;
        GameController.getInstance().scaleY = height / 1080;
        GameController.getInstance().scaleSetting();

        GameController.getInstance().setScreenSize(width, height);
    }

    @Override
    public void pause() {
        GameController.Debug("MyGame - pause()");
    }

    @Override
    public void resume() {
        GameController.Debug("MyGame - resume()");
    }

    @Override
    public void dispose() {
        GameController.Debug("MyGame - dispose()");
        batch.dispose();
        titleFont.dispose();
        GameController.getInstance().release();
    }

    public void getMyCard() {
        ArrayList<Card> myCard = new ArrayList<Card>();
        for (int i = 0; i < 20; i++) {
            Card card = new Card();
            card.index = i + 1;
            card.level = i + 1;
            card.name = "card " + i;
            card.hp = 10 + i;
            card.attack = 5 + i;
            card.count = i + 1;
            card.battlePoint = (card.hp + card.attack) * card.level;

            if (i % 2 == 0)
                card.attackType = Card.ATTACK_TYPE_NEAR;
            else
                card.attackType = Card.ATTACK_TYPE_DISTANCE;

            if (i % 4 == 0)
                card.rareType = Card.CARD_RARE_TYPE_NOMAL;
            else if (i % 4 == 1)
                card.rareType = Card.CARD_RARE_TYPE_RARE;
            else if (i % 4 == 2)
                card.rareType = Card.CARD_RARE_TYPE_UNIQUE;
            else if (i % 4 == 3)
                card.rareType = Card.CARD_RARE_TYPE_LEGEND;


            myCard.add(card);
        }

        GameController.getInstance().getBoss().setMyCardList(myCard);


        getMyBattleCard();
    }

    public void getMyBattleCard() {
        ArrayList<Card> myBattleCard = new ArrayList<Card>();
        for (int i = 0; i < 8; i++) {
            myBattleCard.add(GameController.getInstance().getBoss().getMyCardList().get(i));
        }
        GameController.getInstance().getBoss().setMyBattleCardList(myBattleCard);

    }

}
