package kr.codewiz.dd3k.controller;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Timer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Random;

import kr.codewiz.dd3k.data.Card;
import kr.codewiz.dd3k.data.Unit;
import kr.codewiz.dd3k.listener.AndroidHandler;


public class BattleController {

    private static BattleController instance;


    public static BattleController getInstance() {
        if (instance == null) {
            instance = new BattleController();
        }
        return instance;
    }

    public HashMap<Integer, Card> battleCards = new HashMap<Integer, Card>();
    public HashMap<Integer, Card> waitCards = new HashMap<Integer, Card>();

    public HashMap<Integer, Card> enermyBattleCards = new HashMap<Integer, Card>();
    public HashMap<Integer, Card> enermyWaitCards = new HashMap<Integer, Card>();

    int attackIndex = 0;
    int enermyAttackIndex = 0;

    public void battleReady() {

        for (int i = 0; i < 8; i++) {
            if (i < 4) {
                battleCards.put(GameController.getInstance().getBoss().getMyBattleCardList().get(i).index
                        , GameController.getInstance().getBoss().getMyBattleCardList().get(i));
            } else {
                waitCards.put(GameController.getInstance().getBoss().getMyBattleCardList().get(i).index
                        , GameController.getInstance().getBoss().getMyBattleCardList().get(i));
            }
        }

    }

    public void enermyBattleReady() {

        for (int i = 0; i < 8; i++) {
            if (i < 4) {
                enermyBattleCards.put(GameController.getInstance().getEnermyBoss().getMyBattleCardList().get(i).index
                        , GameController.getInstance().getEnermyBoss().getMyBattleCardList().get(i));
            } else {
                enermyWaitCards.put(GameController.getInstance().getEnermyBoss().getMyBattleCardList().get(i).index
                        , GameController.getInstance().getEnermyBoss().getMyBattleCardList().get(i));
            }
        }

    }

    public void battleCard(int index) {
        attackIndex = index;
        waitCards.put(battleCards.get(index).index, battleCards.get(index));
        battleCards.remove(index);

    }


    public Card cardReady() {

        Object key = null;
        int readyIndex = 0;
        while (key == null || attackIndex == waitCards.get(key).index) {
            Object[] Keys = waitCards.keySet().toArray();
            key = Keys[new Random().nextInt(Keys.length)];
            GameController.Debug("BattleController - cardReady(waitCards.get(key).index) : " + waitCards.get(key).index);
            readyIndex = waitCards.get(key).index;
        }

        battleCards.put(readyIndex, waitCards.get(readyIndex));
        waitCards.remove(readyIndex);

        return battleCards.get(readyIndex);

    }


    public void enermyBattleCard(int index) {

        GameController.Debug("BattleController - enermyBattleCard(index) : " + index);
        enermyAttackIndex = index;
        enermyWaitCards.put(enermyBattleCards.get(index).index, enermyBattleCards.get(index));
        enermyBattleCards.remove(index);

        enermyCardReady();
    }

    public void enermyCardReady() {

        GameController.Debug("BattleController - enermyCardReady()");
        Object key = null;
        int readyIndex = 0;
        while (key == null || enermyAttackIndex == enermyWaitCards.get(key).index) {
            Object[] Keys = enermyWaitCards.keySet().toArray();
            GameController.Debug("BattleController - enermyCardReady(Keys.length) : " + Keys.length);
            key = Keys[new Random().nextInt(Keys.length)];
            GameController.Debug("BattleController - enermyCardReady(key) : " + key);
            readyIndex = enermyWaitCards.get(key).index;
            GameController.Debug("BattleController - enermyCardReady(waitCards.get(key).index) : " + enermyWaitCards.get(key).index);
        }
        GameController.Debug("BattleController - enermyCardReady(readyIndex) : " + readyIndex);

        enermyBattleCards.put(readyIndex, enermyWaitCards.get(readyIndex));
        enermyWaitCards.remove(readyIndex);

        enermyTurn();

    }


    public static Comparator<Card> battlePointComparator = new Comparator<Card>() {
        public int compare(Card name1, Card name2) {

            if (name1.battlePoint > name1.battlePoint) {
                return -1; // 비교대상보다 작을경우 음수 return
            } else if (name1.battlePoint < name1.battlePoint) {
                return 1; // 비교대상보다 클 경우 양수 return
            } else {
                return 0; // 비교대상과 같을 경우 0 return
            }
        }
    };

    public int cardPosition_X = 10;
    public int cardPosition_Y_1 = 270 + 270 + 270 + 10;
    public int cardPosition_Y_2 = 270 + 270 + 10;
    public int cardPosition_Y_3 = 270 + 10;
    public int cardPosition_Y_4 = 10;


    public int battlePositionfirstX = GameController.getInstance().AREA_BATTLE_CARD_WIDTH
            + GameController.getInstance().AREA_BATTLE_CARD_OPEN_WIDTH
            + GameController.getInstance().AREA_BATTLE_BOSS_WIDTH;

    public void scaleSetting() {
        cardPosition_X *= GameController.getInstance().scaleX;
        cardPosition_Y_1 *= GameController.getInstance().scaleY;
        cardPosition_Y_2 *= GameController.getInstance().scaleY;
        cardPosition_Y_3 *= GameController.getInstance().scaleY;
        cardPosition_Y_4 *= GameController.getInstance().scaleY;

        battlePositionfirstX *= GameController.getInstance().scaleX;


        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 6; j++) {

                Rectangle rectangle = new Rectangle();

                rectangle.setX(battlePositionfirstX
                        + GameController.getInstance().AREA_BATTLE_LINE_WIDTH * j);
                if (i == 0) {
                    rectangle.setY(GameController.getInstance().AREA_BATTLE_BOTTOM
                            + GameController.getInstance().AREA_BATTLE_LINE_HEIGHT * 3);
                } else if (i == 1) {
                    rectangle.setY(GameController.getInstance().AREA_BATTLE_BOTTOM
                            + GameController.getInstance().AREA_BATTLE_LINE_HEIGHT * 2);
                } else if (i == 2) {
                    rectangle.setY(GameController.getInstance().AREA_BATTLE_BOTTOM
                            + GameController.getInstance().AREA_BATTLE_LINE_HEIGHT);
                } else if (i == 3) {
                    rectangle.setY(GameController.getInstance().AREA_BATTLE_BOTTOM);
                }

                rectangle.width = GameController.getInstance().AREA_BATTLE_LINE_WIDTH;
                rectangle.height = GameController.getInstance().AREA_BATTLE_LINE_HEIGHT;

                battlePosition[i][j] = rectangle;

            }
        }

    }


    //    public int[][][] battlePosition = new int[4][6][2];
//    public int[][] battlePosition = new int[4][6];
    public Rectangle[][] battlePosition = new Rectangle[4][6];
    public Unit[][] battleUnit = new Unit[4][6];

    public boolean isMyTurn = false;

    public void myTurn() {
        isMyTurn = true;
        attackUnit.clear();

        for (int i = 0; i < 4; i++) {
            for (int j = 2; j >= 0; j--) {
                if (battleUnit[i][j] != null) {
                    attackUnit.add(battleUnit[i][j]);
                }
            }

        }
        attackStart();
    }

    public void enermySetting() {
        GameController.Debug("BattleController - enermySetting()");
        int attackIndex = 0;
        int attackPoint = 0;
        for (int index : enermyBattleCards.keySet()) {
            GameController.Debug("BattleController - enermySetting(index) : "
                    + index);
            GameController.Debug("BattleController - enermySetting(attackPoint) : "
                    + attackPoint);

            if (attackPoint <= enermyBattleCards.get(index).attack) {
                GameController.Debug("BattleController - enermySetting(enermyBattleCards.get(index).attack) : "
                        + enermyBattleCards.get(index).attack);
                attackPoint = enermyBattleCards.get(index).attack;
                attackIndex = index;
            }

        }

        GameController.Debug("BattleController - enermySetting(attackIndex) : "
                + attackIndex);


        Card card = getCardData(enermyBattleCards.get(attackIndex));

        int[] enermyPosition = unitCheck(card);
        GameController.Debug("BattleController - enermySetting(enermyPosition)_0 : "
                + enermyPosition[0]);
        GameController.Debug("BattleController - enermySetting(enermyPosition)_1 : "
                + enermyPosition[1]);

        battleUnit[enermyPosition[0]][enermyPosition[1]] = new Unit(card
                , (int) BattleController.getInstance().battlePosition[enermyPosition[0]][enermyPosition[1]].x
                , (int) BattleController.getInstance().battlePosition[enermyPosition[0]][enermyPosition[1]].y
                , (int) BattleController.getInstance().battlePosition[enermyPosition[0]][enermyPosition[1]].width
                , (int) BattleController.getInstance().battlePosition[enermyPosition[0]][enermyPosition[1]].height
                , enermyPosition[0], enermyPosition[1]);


        enermyBattleCard(attackIndex);

    }

    public Card getCardData(Card card) {
        Card tmpCard = new Card();

        tmpCard.index = card.index;
        tmpCard.level = card.level;
        tmpCard.name = card.name;
        tmpCard.hp = card.hp;
        tmpCard.attack = card.attack;
        tmpCard.count = card.count;
        tmpCard.battlePoint = card.battlePoint;
        tmpCard.attackType = card.attackType;
        tmpCard.rareType = card.rareType;
        tmpCard.currentHP = card.hp;
        tmpCard.currentAttack = card.attack;

        return tmpCard;

    }


    private int[] unitCheck(Card card) {

        체크

        GameController.Debug("BattleController - unitCheck()");
        int[] enermyPosition = new int[]{
                0, 0
        };

        for (int i = 0; i < 4; i++) {
            for (int j = 2; j >= 0; j--) {
                if (battleUnit[i][j] != null) {
                    GameController.Debug("BattleController - unitCheck(battleUnit[" + i + "][" + j + "])_unit!!");
                    if (card.attackType == Card.ATTACK_TYPE_NEAR) {
                        for (int k = 3; k < 5; k++) {
                            if (battleUnit[i][k] == null && battleUnit[i][3] == null) {
                                enermyPosition[0] = i;
                                enermyPosition[1] = k;
                                GameController.Debug("BattleController - unitCheck(enermyPosition[0]) : " + enermyPosition[0]);
                                GameController.Debug("BattleController - unitCheck(enermyPosition[1]) : " + enermyPosition[1]);
                            }
                            return enermyPosition;
                        }
                    } else if (card.attackType == Card.ATTACK_TYPE_DISTANCE) {
                        for (int k = 4; k < 6; k++) {
                            if (battleUnit[i][k] == null) {
                                enermyPosition[0] = i;
                                enermyPosition[1] = k;
                                GameController.Debug("BattleController - unitCheck(enermyPosition[0]) : " + enermyPosition[0]);
                                GameController.Debug("BattleController - unitCheck(enermyPosition[1]) : " + enermyPosition[1]);
                            }
                            return enermyPosition;
                        }
                    }
                }

            }
        }

        return enermyPosition;

    }


    public void enermyTurn() {
        GameController.Debug("BattleController - enermyTurn()");
        isMyTurn = false;
        for (int i = 0; i < 4; i++) {
            for (int j = 3; j < 6; j++) {
                if (battleUnit[i][j] != null) {
                    attackUnit.add(battleUnit[i][j]);
                }
            }

        }
        attackStart();
    }

    public ArrayList<Unit> attackUnit = new ArrayList<Unit>();

    public void attackStart() {
        GameController.Debug("BattleController - attackStart()");
        if (attackUnit.size() > 0) {
            if (isMyTurn) {
                attackUnit.get(0).attack();
            } else {
                attackUnit.get(0).enermyAttack();
            }
        } else {
            if (isMyTurn) {
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        enermySetting();
                    }
                }, 0.5f);

            } else {
                isMyTurn = true;
            }
        }

    }
}
