package kr.codewiz.dd3k.controller;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

import java.util.ArrayList;

import kr.codewiz.dd3k.data.Boss;
import kr.codewiz.dd3k.data.Card;
import kr.codewiz.dd3k.listener.AndroidHandler;
import kr.codewiz.dd3k.screen.MainScreen;


public class GameController {

    private static GameController instance;
    public static final String TAG = "2D3K";
    public static final boolean isDebug = true;


    public static GameController getInstance() {
        if (instance == null) {
            instance = new GameController();
        }
        return instance;
    }

    public static void Debug(String msg) {
        Gdx.app.log(TAG, msg);
    }

    public static void release() {
        instance = null;
    }

    public static void showDialog(String title, String msg, String hint) {
        Gdx.input.getTextInput(new Input.TextInputListener() {

            @Override
            public void input(String text) {
                removeSystemBar();
            }

            @Override
            public void canceled() {
                removeSystemBar();
            }
        }, title, msg, hint);
    }

    public static void removeSystemBar() {
        GameController.getInstance().getAndroidListener().setMsg(GameController.ANDROID_LISTENER_MSG_SYSTEM_BAR, null);
    }


    public void setAndroidListener(AndroidHandler listener) {
        androidListener = listener;
    }

    public AndroidHandler getAndroidListener() {
        return androidListener;
    }

    public AndroidHandler androidListener;

    public float scaleX = 1.0f;
    public float scaleY = 1.0f;

    public void scaleSetting() {
        AREA_BATTLE_CARD_WIDTH *= scaleX;
        AREA_BATTLE_CARD_OPEN_WIDTH *= scaleX;
        AREA_BATTLE_BOSS_WIDTH *= scaleX;
        AREA_BATTLE_BOTTOM *= scaleX;
        AREA_BATTLE_LINE_WIDTH *= scaleX;
        AREA_BATTLE_LINE_HEIGHT *= scaleY;

        AREA_BATTLE_LINE_START_POSITION[0] = AREA_BATTLE_CARD_WIDTH + AREA_BATTLE_CARD_OPEN_WIDTH + AREA_BATTLE_BOSS_WIDTH;
        AREA_BATTLE_LINE_START_POSITION[1] = AREA_BATTLE_BOTTOM;

        BattleController.getInstance().scaleSetting();
    }


    public static final int ANDROID_LISTENER_MSG_SYSTEM_BAR = 100;
    public static final int ANDROID_LISTENER_MSG_PRESS_BACK_KEY = 101;

    public int AREA_BATTLE_CARD_WIDTH = 208;
    public int AREA_BATTLE_CARD_OPEN_WIDTH = 50;
    public int AREA_BATTLE_BOSS_WIDTH = 231;

    public int AREA_BATTLE_BOTTOM = 50;
    public int AREA_BATTLE_LINE_WIDTH = 200;
    public int AREA_BATTLE_LINE_HEIGHT = 200;

    public int[] AREA_BATTLE_LINE_START_POSITION = {AREA_BATTLE_CARD_WIDTH + AREA_BATTLE_CARD_OPEN_WIDTH + AREA_BATTLE_BOSS_WIDTH, AREA_BATTLE_BOTTOM};

    public static final int WIDTH_POSITION = 0;
    public static final int HEIGHT_POSITION = 1;

    private int[] screenSize = new int[2];

    public void setScreenSize(int width, int height) {
        screenSize[WIDTH_POSITION] = width;
        screenSize[HEIGHT_POSITION] = height;
    }

    public int getScreentSize(int position) {
        return screenSize[position];
    }

    private Boss boss = null;

    public void setBoss(Boss boss) {
        this.boss = boss;
    }

    public Boss getBoss() {
        return this.boss;
    }

    private Boss enermyBoss = null;

    public void setEnermyBoss(Boss boss) {
        this.enermyBoss = boss;
    }

    public Boss getEnermyBoss() {
        return this.enermyBoss;
    }


    private MainScreen mainScreen;

    public MainScreen getMainScreen() {
        return mainScreen;
    }

    public void setMainScreen(MainScreen mainScreen) {
        this.mainScreen = mainScreen;
    }

}
