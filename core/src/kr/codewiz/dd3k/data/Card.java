package kr.codewiz.dd3k.data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;

import kr.codewiz.dd3k.controller.BattleController;
import kr.codewiz.dd3k.controller.GameController;

public class Card {

    public int index = 0;
    public int level = 1;
    public String name = "";
    public int hp = 0;
    public int attack = 0;
    public int currentHP = 0;
    public int currentAttack = 0;
    public int count = 0;
    public int battlePoint = 0;
    public int defence = 0;
    public boolean isShield = false;
    public int attackType = ATTACK_TYPE_NEAR;

    public static final int ATTACK_TYPE_NEAR = 0;
    public static final int ATTACK_TYPE_DISTANCE = 1;

    public static final int CARD_RARE_TYPE_NOMAL = 0;
    public static final int CARD_RARE_TYPE_RARE = 1;
    public static final int CARD_RARE_TYPE_UNIQUE = 2;
    public static final int CARD_RARE_TYPE_LEGEND = 3;

    public int rareType = CARD_RARE_TYPE_NOMAL;
    private SpriteBatch spriteBatch;

    private int x = 0, y = 0, width = 188, height = 250;
    private int originX = 0, originY = 0;

    public boolean isClicked = false;

    Texture texture;

    public BitmapFont title_BF;
    public BitmapFont content_BF;

    public Card(int x, int y) {
        setBounds(x, y, width *= GameController.getInstance().scaleX, height *= GameController.getInstance().scaleY);
        originX = x;
        originY = y;

//        texture = new Texture(Gdx.files.internal("card_1.jpg"));
        spriteBatch = new SpriteBatch();

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font/arial.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 30;
        parameter.color = Color.BLACK;
        title_BF = generator.generateFont(parameter);
        parameter.size = 20;
        content_BF = generator.generateFont(parameter);
        generator.dispose();


    }

    public void setData(Card card) {
        index = card.index;
        level = card.level;
        name = card.name;
        hp = card.hp;
        attack = card.attack;
        count = card.count;
        battlePoint = card.battlePoint;
        attackType = card.attackType;
        rareType = card.rareType;
        currentHP = card.hp;
        currentAttack = card.attack;

        if (rareType == CARD_RARE_TYPE_NOMAL) {
            texture = new Texture(Gdx.files.internal("card_1.png"));
        } else if (rareType == CARD_RARE_TYPE_RARE) {
            texture = new Texture(Gdx.files.internal("card_2.png"));
        } else if (rareType == CARD_RARE_TYPE_UNIQUE) {
            texture = new Texture(Gdx.files.internal("card_3.png"));
        } else if (rareType == CARD_RARE_TYPE_LEGEND) {
            texture = new Texture(Gdx.files.internal("card_4.png"));
        }

        GameController.Debug("Card - setData(name) : " + name);

    }

    public Card() {

    }

    public void setBounds(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void click() {
        if (!isClicked) {
            x += GameController.getInstance().AREA_BATTLE_CARD_OPEN_WIDTH;
            isClicked = true;
        } else {
            // show card detail.
        }
    }

    public void cancel() {
        if (!isClicked) return;
        isClicked = false;
        x = originX;
    }

    public void render() {

//        GameController.Debug("Unit - render(getDeltaTime) : " + Gdx.graphics.getDeltaTime());

        spriteBatch.begin();

        if (!isResetting) {

            if (isCardResetting && y > originY) {
                y -= 60;
            } else {
                isCardResetting = false;
                y = originY;
            }

            spriteBatch.draw(texture, x, y, width, height);
            title_BF.draw(spriteBatch, name, x + 20, y + height - 20, width - 40, Align.center, true);
            content_BF.draw(spriteBatch, "HP : " + hp + "\nATTACK : " + attack
                    , x + 20, y + 40, width - 40, Align.bottomRight, true);

        }

        spriteBatch.end();
    }


    public Rectangle getRectangle() {
        return new Rectangle(x, y, width, height);
    }

    public void dispose() {
        texture.dispose();
        title_BF.dispose();
    }

    boolean isResetting = false;

    public void reSetting() {
        isResetting = true;
        isClicked = false;

        x = originX;
        y = GameController.getInstance().getScreentSize(GameController.HEIGHT_POSITION);

        BattleController.getInstance().battleCard(index);
        setData(BattleController.getInstance().cardReady());

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                isResetting = false;
                cardResetting();
            }
        }, 0.1f);


    }

    boolean isCardResetting = false;

    private void cardResetting() {
        isCardResetting = true;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }


}
