package kr.codewiz.dd3k.data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;

import kr.codewiz.dd3k.controller.BattleController;
import kr.codewiz.dd3k.controller.GameController;

public class Unit {

    public static final int ANIMATION_TYPE_NOMAL = 0;
    public static final int ANIMATION_TYPE_ATTACK = 1;
    public static final int ANIMATION_TYPE_DEMAGE = 2;
    public static final int ANIMATION_TYPE_DEAD = 3;

    private int state = ANIMATION_TYPE_NOMAL;

    private int x = 0, y = 0, width = 0, height = 0;
    private int originX = 0, originY = 0;

    Animation<TextureRegion> walkAnimation;
    Animation<TextureRegion> backWalkAnimation;

    private float stateTime;
    private SpriteBatch spriteBatch;

    private Card data;

    private int positionX = 0;
    private int positionY = 0;
    public BitmapFont content_BF;
    private static final int FRAME_COLS = 6, FRAME_ROWS = 5;

    Texture attackedTexture;


    public Unit(Card card, int x, int y, int width, int height, int positionX, int positionY) {

        setBounds(x, y, width, height);
        this.originX = x;
        this.originY = y;
        this.data = card;

        this.positionX = positionX;
        this.positionY = positionY;

        Texture walkSheet = new Texture(Gdx.files.internal("walkman.png"));
        TextureRegion[][] tmp = TextureRegion.split(walkSheet,
                walkSheet.getWidth() / FRAME_COLS,
                walkSheet.getHeight() / FRAME_ROWS);
        TextureRegion[] walkFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];

        int index = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
            for (int j = 0; j < FRAME_COLS; j++) {
                walkFrames[index++] = tmp[i][j];
            }
        }
        walkAnimation = new Animation<TextureRegion>(0.025f, walkFrames);


        TextureRegion[][] backTmp = TextureRegion.split(walkSheet,
                walkSheet.getWidth() / FRAME_COLS,
                walkSheet.getHeight() / FRAME_ROWS);
        TextureRegion[] backWalkFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];

        index = 0;

        for (int i = FRAME_ROWS - 1; i >= 0; i--) {
            for (int j = FRAME_COLS - 1; j >= 0; j--) {
                backWalkFrames[index++] = backTmp[i][j];
            }
        }

        backWalkAnimation = new Animation<TextureRegion>(0.025f, backWalkFrames);

        stateTime = 0f;
        spriteBatch = new SpriteBatch();

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font/arial.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.color = Color.WHITE;
        parameter.size = 20;
        content_BF = generator.generateFont(parameter);
        generator.dispose();

        attackedTexture = new Texture(Gdx.files.internal("attacked.png"));


    }

    TextureRegion currentFrame;

    public void render() {

//        GameController.Debug("Unit - render(getDeltaTime) : " + Gdx.graphics.getDeltaTime());

        if (attackPositionX != 0) {
            x = attackPositionX;
            state = ANIMATION_TYPE_ATTACK;
            attackPositionX = 0;
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    GameController.Debug("Unit - render(Timer_schedule_1)");
                    x = originX;
                    state = ANIMATION_TYPE_NOMAL;

                    if (attackTargetPositionY == -1) {
                        // 보스공격
                    } else {
                        BattleController.getInstance().battleUnit[positionX][attackTargetPositionY].attacked(data.currentAttack);
                    }

                    if (BattleController.getInstance().attackUnit.size() > 0)
                        BattleController.getInstance().attackUnit.remove(0);

                    BattleController.getInstance().attackStart();


                }
            }, 0.5f);
        } else {

        }

        stateTime += Gdx.graphics.getDeltaTime();

        if (state == ANIMATION_TYPE_NOMAL) {
            currentFrame = walkAnimation.getKeyFrame(stateTime, true);
        } else if (state == ANIMATION_TYPE_ATTACK) {
            currentFrame = backWalkAnimation.getKeyFrame(stateTime, true);
        }

        spriteBatch.begin();
        spriteBatch.draw(currentFrame, x, y, width, height);
        content_BF.draw(spriteBatch, "HP : " + data.currentHP + "\nATTACK : " + data.currentAttack
                , x + 20, y + 40, width - 40, Align.bottomRight, true);


        if (isAttacked) {
//            spriteBatch.draw(attackedTexture
//                    , originX + (int) (GameController.getInstance().AREA_BATTLE_LINE_WIDTH / 2)
//                            - (int) (attackedTexture.getWidth() / 2)
//                    , originY + (int) (GameController.getInstance().AREA_BATTLE_LINE_HEIGHT / 2)
//                            - (int) (attackedTexture.getHeight() / 2)
//                    , attackedTexture.getWidth()
//                    , attackedTexture.getHeight());
            spriteBatch.draw(attackedTexture
                    , originX
                    , originY
                    , GameController.getInstance().AREA_BATTLE_LINE_WIDTH
                    , GameController.getInstance().AREA_BATTLE_LINE_HEIGHT);
        }

        spriteBatch.end();

    }

    boolean isAttacked = false;


    public void setCardData(Card card) {
        this.data = card;
    }

    public Card getName() {
        return data;
    }


    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int[] getBounds() {
        return new int[]{x, y, width, height};
    }

    public Rectangle getRectangle() {
        return new Rectangle(x, y, width, height);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setBounds(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void dispose() {
        GameController.Debug("Unit - dispose()");
        spriteBatch.dispose();
        content_BF.dispose();
        attackedTexture.dispose();
    }

    private int attackTargetPositionY = -1;

    public void attack() {


        if(positionY == 1 && data.attackType == Card.ATTACK_TYPE_NEAR
         && BattleController.getInstance().battleUnit[positionX][2] != null){
            if (BattleController.getInstance().attackUnit.size() > 0)
                BattleController.getInstance().attackUnit.remove(0);

            BattleController.getInstance().attackStart();
            return;
        }

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                GameController.Debug("Unit - attack()");
                for (int i = 3; i < 5; i++) {
                    if (BattleController.getInstance().battleUnit[positionX][i] != null) {
                        setAttackPosition(BattleController.getInstance().battleUnit[positionX][i].x
                                - GameController.getInstance().AREA_BATTLE_LINE_WIDTH);
                        attackTargetPositionY = i;
                        return;
                    }
                }
                attackTargetPositionY = -1;
                attackPositionX = (int) BattleController.getInstance().battlePosition[positionX][5].x;
            }
        }, 0.5f);


    }

    public void enermyAttack() {

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                GameController.Debug("Unit - enermyAttack()");
                for (int i = 2; i >= 0; i--) {
                    if (BattleController.getInstance().battleUnit[positionX][i] != null) {
                        setAttackPosition(BattleController.getInstance().battleUnit[positionX][i].x
                                + GameController.getInstance().AREA_BATTLE_LINE_WIDTH);
                        attackTargetPositionY = i;
                        return;
                    }
                }
                attackTargetPositionY = -1;
                attackPositionX = (int) BattleController.getInstance().battlePosition[positionX][0].x;
            }
        }, 0.5f);
    }

    private int attackPositionX = 0;

    public void setAttackPosition(int attackPositionX) {
        GameController.Debug("Unit - setAttackPosition(attackPositionX) : " + attackPositionX);
        this.attackPositionX = attackPositionX;
    }


    public void attacked(int demage) {
        GameController.Debug("Unit - attacked(demage) : " + demage);
        if (data.isShield) {
            data.isShield = false;
            // 쉴드 삭제
            return;
        }

        if (data.defence > 0) {
            demage = data.defence;
            if (demage < 0)
                demage = 0;
        }
        data.currentHP -= demage;

        GameController.Debug("Unit - attacked(data.currentHP) : " + data.currentHP);
        isAttacked = true;

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                isAttacked = false;
                GameController.Debug("Unit - attacked(isAttacked) : " + isAttacked);
                GameController.Debug("Unit - attacked_end(data.currentHP) : " + data.currentHP);
                if (data.currentHP <= 0)
                    dead();
            }
        }, 0.5f);


    }

    public void dead() {
        GameController.Debug("Unit - dead(positionX / positionY) : " + positionX + " / " + positionY);
        BattleController.getInstance().battleUnit[positionX][positionY] = null;
        this.dispose();
    }

    public void heal(int value, boolean isMaxOver) {

        data.currentHP += value;
        if (!isMaxOver)
            data.currentHP = data.hp;

    }

    public void attackBuff(int value) {
        data.currentAttack += value;
    }

    public void attackNuff(int value) {
        data.currentAttack -= value;

        if (data.currentAttack < 0)
            data.currentAttack = 0;

    }


}