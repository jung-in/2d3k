package kr.codewiz.dd3k.data;

import java.util.ArrayList;

public class Boss {

    public int battlePoint = 0;

    ArrayList<Card> myCardList = new ArrayList<Card>();
    ArrayList<Card> myBattleCardList = new ArrayList<Card>();
    public void setMyCardList(ArrayList<Card> list) {
        myCardList = list;
    }

    public void setMyBattleCardList(ArrayList<Card> list) {
        myBattleCardList = list;
    }
    public ArrayList<Card> getMyCardList() {
        return myCardList;
    }

    public ArrayList<Card> getMyBattleCardList() {
        return myBattleCardList;
    }

}
