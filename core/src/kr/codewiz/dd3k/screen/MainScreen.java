package kr.codewiz.dd3k.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import kr.codewiz.dd3k.MyGame;
import kr.codewiz.dd3k.controller.GameController;

public class MainScreen implements Screen, InputProcessor {
    MyGame game;
    OrthographicCamera camera;
    Texture attack_img;
//    ImageButton playButton;
    Rectangle attack_img_rectangle;

    public MainScreen(final MyGame game) {
        this.game = game;
        camera = new OrthographicCamera();
        camera.setToOrtho(false);

        attack_img = new Texture("btn_battle.png");

        attack_img_rectangle = new Rectangle(GameController.getInstance().getScreentSize(GameController.WIDTH_POSITION)
                - attack_img.getWidth() - 10, 10, attack_img.getWidth(), attack_img.getHeight());

//        Drawable drawable = new TextureRegionDrawable(new TextureRegion(attack_img));
//        playButton = new ImageButton(drawable);
//        playButton.addListener(new ClickListener() {
//
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
//                super.clicked(event, x, y);
//                GameController.Debug("MainScreen - playButton.clicked()");
//                game.setScreen(new BattleScreen(game));
//
//            }
//
////            @Override
////            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
////                super.touchUp(event, x, y, pointer, button);
////                GameController.Debug("MainScreen - playButton.InputListener(touchUp)");
////                game.setScreen(new BattleScreen(game));
////            }
//
//        });
//        playButton.setPosition(GameController.getInstance().getScreentSize(GameController.WIDTH_POSITION)
//                        - attack_img.getWidth() - 10
//                , 10);


        GameController.getInstance().setMainScreen(this);
        Gdx.input.setInputProcessor(this);

//        attack_BTN = new Sprite(attack_img
//                , GameController.getInstance().getScreentSize(GameController.WIDTH_POSITION) - attack_img.getWidth() - 10
//                , attack_img.getHeight() + 10
//                , attack_img.getWidth()
//                , attack_img.getHeight());
//        attack_BTN.setOriginCenter();

    }

    @Override
    public void show() {
        GameController.Debug("MainScreen - show()");
//        Gdx.input.setInputProcessor(this.stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 1, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.draw(attack_img
                , GameController.getInstance().getScreentSize(GameController.WIDTH_POSITION) - attack_img.getWidth() - 10
                , 10);
//        playButton.draw(game.batch, 1);

        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {
        GameController.Debug("MainScreen - resize()");
    }

    @Override
    public void pause() {
        GameController.Debug("MainScreen - pause()");
    }

    @Override
    public void resume() {
        GameController.Debug("MainScreen - resume()");
    }

    @Override
    public void hide() {
        GameController.Debug("MainScreen - hide()");
    }

    @Override
    public void dispose() {
        GameController.Debug("MainScreen - dispose()");
        attack_img.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK) {
            GameController.Debug("MainScreen - keyDown(Input.Keys.BACK)");
            GameController.getInstance().getAndroidListener().setMsg(GameController.ANDROID_LISTENER_MSG_PRESS_BACK_KEY, null);
            return true;
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        Vector3 tmp = camera.unproject(new Vector3(screenX, screenY, 0));
        if (pointer == 0) {
            if (attack_img_rectangle.contains(tmp.x, tmp.y)) {
                GameController.Debug("MainScreen - touchUp(attack)");
                game.setScreen(new BattleScreen(game));
            }
        }

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
