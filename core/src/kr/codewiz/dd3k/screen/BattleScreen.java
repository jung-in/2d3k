package kr.codewiz.dd3k.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Iterator;

import kr.codewiz.dd3k.MyGame;
import kr.codewiz.dd3k.controller.BattleController;
import kr.codewiz.dd3k.controller.GameController;
import kr.codewiz.dd3k.data.Boss;
import kr.codewiz.dd3k.data.Card;
import kr.codewiz.dd3k.data.Unit;

public class BattleScreen implements Screen, InputProcessor {
    MyGame game;
    OrthographicCamera camera;
    Card card_1;
    Card card_2;
    Card card_3;
    Card card_4;
//    Unit unit;

    Texture arrow;
    SpriteBatch arrowSpriteBatch;


    public BattleScreen(MyGame game) {

        GameController.Debug("BattleScreen()");
        GameController.getInstance().setEnermyBoss(new Boss());
        GameController.getInstance().getEnermyBoss().battlePoint = 100;


        ArrayList<Card> enermyBattleCard = new ArrayList<Card>();
        for (int i = 0; i < 8; i++) {
            enermyBattleCard.add(BattleController.getInstance().getCardData(GameController.getInstance().getBoss().getMyCardList().get(i)));
        }

        GameController.getInstance().getEnermyBoss().setMyBattleCardList(enermyBattleCard);

        this.game = game;

        camera = new OrthographicCamera();
        camera.setToOrtho(false);

//        unit = new Unit(new Card(), 50, 100, 200, 200);

        BattleController.getInstance().battleReady();
        BattleController.getInstance().enermyBattleReady();


        card_1 = new Card(BattleController.getInstance().cardPosition_X, BattleController.getInstance().cardPosition_Y_1);
        card_2 = new Card(BattleController.getInstance().cardPosition_X, BattleController.getInstance().cardPosition_Y_2);
        card_3 = new Card(BattleController.getInstance().cardPosition_X, BattleController.getInstance().cardPosition_Y_3);
        card_4 = new Card(BattleController.getInstance().cardPosition_X, BattleController.getInstance().cardPosition_Y_4);

        int settingIndex = 0;
        for (int index : BattleController.getInstance().battleCards.keySet()) {
            if (settingIndex == 0) {
                card_1.setData(BattleController.getInstance().battleCards.get(index));

            } else if (settingIndex == 1) {
                card_2.setData(BattleController.getInstance().battleCards.get(index));

            } else if (settingIndex == 2) {
                card_3.setData(BattleController.getInstance().battleCards.get(index));

            } else if (settingIndex == 3) {
                card_4.setData(BattleController.getInstance().battleCards.get(index));
            }
            settingIndex++;
        }


        textureBounds_1 = new Rectangle(card_1.getRectangle());
        textureBounds_2 = new Rectangle(card_2.getRectangle());
        textureBounds_3 = new Rectangle(card_3.getRectangle());
        textureBounds_4 = new Rectangle(card_4.getRectangle());


        arrowSpriteBatch = new SpriteBatch();

        arrow = new Texture(Gdx.files.internal("arrow.png"));

        Gdx.input.setInputProcessor(this);

        if (GameController.getInstance().getBoss().battlePoint
                >= GameController.getInstance().getEnermyBoss().battlePoint)
            BattleController.getInstance().isMyTurn = true;
        else {
            BattleController.getInstance().isMyTurn = false;
            BattleController.getInstance().enermySetting();
        }
        bossSetting();
    }

    Texture bossTexture;
    Texture enermybossTexture;

    private void bossSetting() {
        bossTexture = new Texture(Gdx.files.internal("ubi.jpeg"));
        enermybossTexture = new Texture(Gdx.files.internal("jojo.jpeg"));

        bossSpriteBatch = new SpriteBatch();
    }

    private SpriteBatch bossSpriteBatch;

    @Override
    public void show() {
        GameController.Debug("BattleScreen - show()");
    }


    Rectangle textureBounds_1;
    Rectangle textureBounds_2;
    Rectangle textureBounds_3;
    Rectangle textureBounds_4;

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        drawBattleLine();

        card_1.render();
        card_2.render();
        card_3.render();
        card_4.render();


        bossSpriteBatch.begin();
        bossSpriteBatch.draw(bossTexture, GameController.getInstance().AREA_BATTLE_CARD_WIDTH
                        + GameController.getInstance().AREA_BATTLE_CARD_OPEN_WIDTH + 10
                , GameController.getInstance().getScreentSize(1) - 160 - 295, 211, 295);
        bossSpriteBatch.draw(enermybossTexture, GameController.getInstance().getScreentSize(0) - 221
                , GameController.getInstance().getScreentSize(1) - 160 - 295, 211, 295);
        bossSpriteBatch.end();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 6; j++) {
                if (BattleController.getInstance().battleUnit[i][j] != null)
                    BattleController.getInstance().battleUnit[i][j].render();
            }
        }

        if (clickAttackType != CLICK_NONE) {
            arrowSpriteBatch.begin();
            for (int i = 0; i < 4; i++) {

                if (BattleController.getInstance().battleUnit[i][1] == null) {
                    arrowSpriteBatch.draw(arrow, (int) BattleController.getInstance().battlePosition[i][1].x
                            , (int) BattleController.getInstance().battlePosition[i][1].y
                            , GameController.getInstance().AREA_BATTLE_LINE_WIDTH
                            , GameController.getInstance().AREA_BATTLE_LINE_HEIGHT);
                }

            }


            if (clickAttackType == CLICK_ATTACK_TYPE_NEAR) {
                for (int i = 0; i < 4; i++) {
                    if (BattleController.getInstance().battleUnit[i][2] == null) {
                        arrowSpriteBatch.draw(arrow, (int) BattleController.getInstance().battlePosition[i][2].x
                                , (int) BattleController.getInstance().battlePosition[i][2].y
                                , GameController.getInstance().AREA_BATTLE_LINE_WIDTH
                                , GameController.getInstance().AREA_BATTLE_LINE_HEIGHT);
                    }

                }
            }

            if (clickAttackType == CLICK_ATTACK_TYPE_DISTANCE) {
                for (int i = 0; i < 4; i++) {
                    if (BattleController.getInstance().battleUnit[i][0] == null) {
                        arrowSpriteBatch.draw(arrow, (int) BattleController.getInstance().battlePosition[i][0].x
                                , (int) BattleController.getInstance().battlePosition[i][0].y
                                , GameController.getInstance().AREA_BATTLE_LINE_WIDTH
                                , GameController.getInstance().AREA_BATTLE_LINE_HEIGHT);
                    }

                }
            }

            arrowSpriteBatch.end();
        }


    }


    private void drawBattleLine() {
        ShapeRenderer sr = new ShapeRenderer();
        sr.setColor(Color.WHITE);
        sr.setProjectionMatrix(camera.combined);

        sr.begin(ShapeRenderer.ShapeType.Filled);

        for (int i = 0; i < 7; i++) {
            sr.rectLine(GameController.getInstance().AREA_BATTLE_LINE_START_POSITION[0]
                            + (GameController.getInstance().AREA_BATTLE_LINE_WIDTH * i)
                    , GameController.getInstance().AREA_BATTLE_LINE_START_POSITION[1],
                    GameController.getInstance().AREA_BATTLE_LINE_START_POSITION[0]
                            + (GameController.getInstance().AREA_BATTLE_LINE_WIDTH * i)
                    , GameController.getInstance().AREA_BATTLE_LINE_START_POSITION[1]
                            + GameController.getInstance().AREA_BATTLE_LINE_HEIGHT * 4, 2);
        }

        for (int i = 0; i < 5; i++) {
            sr.rectLine(GameController.getInstance().AREA_BATTLE_LINE_START_POSITION[0]
                    , GameController.getInstance().AREA_BATTLE_LINE_START_POSITION[1]
                            + (GameController.getInstance().AREA_BATTLE_LINE_HEIGHT * i),
                    GameController.getInstance().AREA_BATTLE_LINE_START_POSITION[0]
                            + GameController.getInstance().AREA_BATTLE_LINE_WIDTH * 6
                    , GameController.getInstance().AREA_BATTLE_LINE_START_POSITION[1]
                            + (GameController.getInstance().AREA_BATTLE_LINE_HEIGHT * i), 2);
        }

        drawArea(sr);

        sr.end();
    }


    private void drawArea(ShapeRenderer sr) {
        sr.rectLine(new Vector2(208, 0), new Vector2(208, GameController.getInstance().getScreentSize(1)), 1);
        sr.rectLine(new Vector2(208 + 50, 0), new Vector2(208 + 50, GameController.getInstance().getScreentSize(1)), 2);

        for (int i = 1; i < 4; i++) {
            sr.rectLine(new Vector2(0, 270 * i), new Vector2(208 + 50, 270 * i), 2);
        }

        sr.rectLine(new Vector2(208 + 50, GameController.getInstance().AREA_BATTLE_LINE_START_POSITION[1]
                        + (GameController.getInstance().AREA_BATTLE_LINE_HEIGHT * 4) + 80)
                , new Vector2(GameController.getInstance().AREA_BATTLE_LINE_START_POSITION[0]
                        + (GameController.getInstance().AREA_BATTLE_LINE_WIDTH * 6)
                        , GameController.getInstance().AREA_BATTLE_LINE_START_POSITION[1]
                        + (GameController.getInstance().AREA_BATTLE_LINE_HEIGHT * 4) + 80), 2);
    }

    @Override
    public void resize(int width, int height) {
        GameController.Debug("BattleScreen - resize(width / height) : " + width + " / " + height);
    }

    @Override
    public void pause() {
        GameController.Debug("BattleScreen - pause()");
    }

    @Override
    public void resume() {
        GameController.Debug("BattleScreen - resume()");
    }

    @Override
    public void hide() {
        GameController.Debug("BattleScreen - hide()");
    }

    @Override
    public void dispose() {
        GameController.Debug("BattleScreen - dispose()");
        card_1.dispose();
        card_2.dispose();
        card_3.dispose();
        card_4.dispose();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 6; j++) {
                if (BattleController.getInstance().battleUnit[i][j] != null) {

                    BattleController.getInstance().battleUnit[i][j].dispose();
                    BattleController.getInstance().battleUnit[i][j] = null;
                }
            }
        }


//        stage.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK) {
            GameController.Debug("BattleScreen - keyDown(Input.Keys.BACK)");
            game.setScreen(GameController.getInstance().getMainScreen());
            Gdx.input.setInputProcessor(GameController.getInstance().getMainScreen());
            dispose();
            return true;
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }


    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        Vector3 tmp = camera.unproject(new Vector3(screenX, screenY, 0));
        if (pointer == 0) {

            if (!BattleController.getInstance().isMyTurn) return false;

            if (textureBounds_1.contains(tmp.x, tmp.y)) {
                GameController.Debug("BattleScreen - touchUp(card_1)");

                card_1.click();
                card_2.cancel();
                card_3.cancel();
                card_4.cancel();

                cardClick(card_1);
            } else if (textureBounds_2.contains(tmp.x, tmp.y)) {
                GameController.Debug("BattleScreen - touchUp(card_2)");

                card_2.click();
                card_1.cancel();
                card_3.cancel();
                card_4.cancel();

                cardClick(card_2);
            } else if (textureBounds_3.contains(tmp.x, tmp.y)) {
                GameController.Debug("BattleScreen - touchUp(card_3)");

                card_3.click();
                card_1.cancel();
                card_2.cancel();
                card_4.cancel();

                cardClick(card_3);
            } else if (textureBounds_4.contains(tmp.x, tmp.y)) {
                GameController.Debug("BattleScreen - touchUp(card_4)");

                card_4.click();
                card_1.cancel();
                card_2.cancel();
                card_3.cancel();

                cardClick(card_4);
            }

            battleAreaClickCheck(tmp.x, tmp.y);
        }

        return true;
    }

    private void cardClick(Card card) {
        if (card.attackType == Card.ATTACK_TYPE_NEAR) {
            guideSetting(CLICK_ATTACK_TYPE_NEAR);
        } else if (card.attackType == Card.ATTACK_TYPE_DISTANCE) {
            guideSetting(CLICK_ATTACK_TYPE_DISTANCE);
        } else {
            guideSetting(CLICK_NONE);
        }
    }


    private void battleAreaClickCheck(float x, float y) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 6; j++) {
                if (BattleController.getInstance().battlePosition[i][j].contains(x, y)) {
                    GameController.Debug("BattleScreen - touchUp_battleAreaClickCheck(i . j) : " + i + " . " + j);

                    if (BattleController.getInstance().battleUnit[i][j] == null
                            && j < 3) {

                        if (card_1.isClicked) {
                            if (!unitSettingCheck(card_1.attackType, j))
                                return;
                            guideSetting(CLICK_NONE);

                            BattleController.getInstance().battleUnit[i][j] = new Unit(BattleController.getInstance().getCardData(card_1)
                                    , (int) BattleController.getInstance().battlePosition[i][j].x
                                    , (int) BattleController.getInstance().battlePosition[i][j].y
                                    , (int) BattleController.getInstance().battlePosition[i][j].width
                                    , (int) BattleController.getInstance().battlePosition[i][j].height
                                    , i, j);

                            card_1.reSetting();
                            BattleController.getInstance().myTurn();
                        } else if (card_2.isClicked) {
                            if (!unitSettingCheck(card_2.attackType, j))
                                return;
                            guideSetting(CLICK_NONE);

                            BattleController.getInstance().battleUnit[i][j] = new Unit(BattleController.getInstance().getCardData(card_2)
                                    , (int) BattleController.getInstance().battlePosition[i][j].x
                                    , (int) BattleController.getInstance().battlePosition[i][j].y
                                    , (int) BattleController.getInstance().battlePosition[i][j].width
                                    , (int) BattleController.getInstance().battlePosition[i][j].height
                                    , i, j);
                            card_2.reSetting();
                            BattleController.getInstance().myTurn();
                        } else if (card_3.isClicked) {
                            if (!unitSettingCheck(card_3.attackType, j))
                                return;
                            guideSetting(CLICK_NONE);

                            BattleController.getInstance().battleUnit[i][j] = new Unit(BattleController.getInstance().getCardData(card_3)
                                    , (int) BattleController.getInstance().battlePosition[i][j].x
                                    , (int) BattleController.getInstance().battlePosition[i][j].y
                                    , (int) BattleController.getInstance().battlePosition[i][j].width
                                    , (int) BattleController.getInstance().battlePosition[i][j].height
                                    , i, j);
                            card_3.reSetting();
                            BattleController.getInstance().myTurn();
                        } else if (card_4.isClicked) {
                            if (!unitSettingCheck(card_4.attackType, j))
                                return;
                            guideSetting(CLICK_NONE);

                            BattleController.getInstance().battleUnit[i][j] = new Unit(BattleController.getInstance().getCardData(card_4)
                                    , (int) BattleController.getInstance().battlePosition[i][j].x
                                    , (int) BattleController.getInstance().battlePosition[i][j].y
                                    , (int) BattleController.getInstance().battlePosition[i][j].width
                                    , (int) BattleController.getInstance().battlePosition[i][j].height
                                    , i, j);
                            card_4.reSetting();
                            BattleController.getInstance().myTurn();
                        }

//                        card_1.cancel();
//                        card_2.cancel();
//                        card_3.cancel();
//                        card_4.cancel();
                    } else {
                        if (BattleController.getInstance().battleUnit[i][j] != null) {
                            if (BattleController.getInstance().battleUnit[i][j].getState() == Unit.ANIMATION_TYPE_NOMAL)
                                BattleController.getInstance().battleUnit[i][j].setState(Unit.ANIMATION_TYPE_ATTACK);
                            else if (BattleController.getInstance().battleUnit[i][j].getState() == Unit.ANIMATION_TYPE_ATTACK)
                                BattleController.getInstance().battleUnit[i][j].setState(Unit.ANIMATION_TYPE_NOMAL);
                        }

                    }


                }
            }
        }

    }


    private boolean unitSettingCheck(int cardType, int position) {

        if (cardType == Card.ATTACK_TYPE_NEAR) {
            if (position == 1 || position == 2)
                return true;
        } else if (cardType == Card.ATTACK_TYPE_DISTANCE) {
            if (position == 0 || position == 1)
                return true;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    private final int CLICK_NONE = 0;
    private final int CLICK_ATTACK_TYPE_NEAR = 1;
    private final int CLICK_ATTACK_TYPE_DISTANCE = 2;
    private int clickAttackType = CLICK_NONE;

    private void guideSetting(int type) {
        clickAttackType = type;
    }
}
