package kr.codewiz.dd3k.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

import kr.codewiz.dd3k.MyGame;
import kr.codewiz.dd3k.controller.GameController;

public class IntroScreen implements Screen {
    MyGame game;
    OrthographicCamera camera;
//    Unit unit;
    public IntroScreen(MyGame game) {
        this.game = game;

        camera = new OrthographicCamera();
        camera.setToOrtho(false);
    }

    GlyphLayout fontLayout;

    @Override
    public void show() {
        GameController.Debug("IntroScreen - show()");
        fontLayout = new GlyphLayout();
//        unit = new Unit(new Card());
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();

        fontLayout.setText(game.titleFont, "Welcome to Game!!");
        game.titleFont.draw(game.batch, fontLayout
                , GameController.getInstance().getScreentSize(GameController.WIDTH_POSITION) / 2 - fontLayout.width / 2
                , GameController.getInstance().getScreentSize(GameController.HEIGHT_POSITION) / 2 + fontLayout.height / 2);


        game.batch.end();

        if (Gdx.input.isTouched()) {
            GameController.Debug("IntroScreen - render(Gdx.input.isTouched())");
            game.setScreen(new MainScreen(game));
            dispose();
        }

//        unit.show();
    }

    @Override
    public void resize(int width, int height) {
        GameController.Debug("IntroScreen - resize(width / height) : " + width + " / " + height);
    }

    @Override
    public void pause() {
        GameController.Debug("IntroScreen - pause()");
    }

    @Override
    public void resume() {
        GameController.Debug("IntroScreen - resume()");
    }

    @Override
    public void hide() {
        GameController.Debug("IntroScreen - hide()");
    }

    @Override
    public void dispose() {
        GameController.Debug("IntroScreen - dispose()");
    }
}
