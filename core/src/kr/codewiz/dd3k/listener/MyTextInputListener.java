package kr.codewiz.dd3k.listener;

import com.badlogic.gdx.Input;

import kr.codewiz.dd3k.controller.GameController;

public class MyTextInputListener implements Input.TextInputListener {
    @Override
    public void input(String text) {
        GameController.Debug("MyTextInputListener - input(text) : " + text);
    }

    @Override
    public void canceled() {

    }
}
