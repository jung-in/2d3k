package kr.codewiz.dd3k;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import kr.codewiz.dd3k.listener.AndroidHandler;
import kr.codewiz.dd3k.controller.GameController;

public class AndroidLauncher extends AndroidApplication {

    AlertDialog finishDialog;

    MyGame myGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSystemUI();
        makeFinishDialog();

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        myGame = new MyGame();

        initialize(myGame, config);

        GameController.getInstance().setAndroidListener(new AndroidHandler() {
            @Override
            public void setMsg(int msg, Object obj) {
                handler.obtainMessage(msg, obj).sendToTarget();
            }
        });
    }

    Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message message) {
            Log.d(GameController.TAG, "AndroidLauncher - handleMessage(message.what) : " + message.what);
            if (message.what == GameController.ANDROID_LISTENER_MSG_SYSTEM_BAR) {
                setSystemUI();
            } else if (message.what == GameController.ANDROID_LISTENER_MSG_PRESS_BACK_KEY) {
                if (finishDialog != null && !finishDialog.isShowing())
                    finishDialog.show();
            }
        }
    };


    private void makeFinishDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("finish?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                exit();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setSystemUI();
            }
        });
        finishDialog = builder.create();

    }

    public void setSystemUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            );
        } else
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
            );

    }


    @Override
    public void onBackPressed() {
        Log.d(GameController.TAG, "AndroidLauncher - onBackPressed()");
//        handler.obtainMessage(GameController.ANDROID_LISTENER_MSG_PRESS_BACK_KEY, null).sendToTarget();
    }


}
